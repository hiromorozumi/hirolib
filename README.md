# hirolib #

A small library of useful I/O and other functions to make cross-platform development easier. Mostly used for my own trivial personal development. Still under development... more functions will be added as I go.

### Features ###

* Keyboard input functions
* Timer functions
* filesystem functions

### How to Use ###

The header file hirolib.h lists the functions available for use. To use this library, just compile your program with hirolib.cpp. Note that hirolib.cpp needs to come first in the list of .cpp files in your compilation command.

### Contact ###

You are welcome to contact regarding this library at hirolib-at-hiromorozumi-dot-com.