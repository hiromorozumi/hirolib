#ifndef HIROLIB_H
#define HIROLIB_H

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>

using namespace std;

#if defined (_WIN32)

	#include <windows.h>
	#include <conio.h>
	
#elif defined (__APPLE__) || defined (__linux__)

	#include <cstdlib>
	#include <unistd.h>
	#include <sys/select.h>
	#include <sys/time.h>
	#include <sys/types.h>
	#include <sys/stat.h>
	#include <dirent.h>	
	#include <termios.h>
	#include <time.h>
	#include <pwd.h>
	#include <cstring>

#endif

//////////////////////////////////////////////////////////////////////////////////

namespace kbd
{		
	//////////////////////////////////////////////////////////////////////
	//
	//		windows
	//
	//////////////////////////////////////////////////////////////////////

#if defined (_WIN32)
	
	bool waitOn = false;
	bool breakRequested = false;
	
	void waitForRelease()
	{ waitOn = true; }
	
	void noWaitForRelease()
	{ waitOn = false; }
	
	bool up()
	{	 if(GetAsyncKeyState(VK_UP) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(VK_UP)){} }; return true; } else return false; }	
	bool down()
	{	 if(GetAsyncKeyState(VK_DOWN) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(VK_DOWN)){} }; return true; } else return false; }	
	bool left()
	{	 if(GetAsyncKeyState(VK_LEFT) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(VK_LEFT)){} }; return true; } else return false; }	
	bool right()
	{	 if(GetAsyncKeyState(VK_RIGHT) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(VK_RIGHT)){} }; return true; } else return false; }	
	bool space()
	{	 if(GetAsyncKeyState(VK_SPACE) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(VK_SPACE)){} }; return true; } else return false; }		
	bool ret()
	{	 if(GetAsyncKeyState(VK_RETURN) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(VK_RETURN)){} }; return true; } else return false; }	
	bool ctrl()
	{	 if(GetAsyncKeyState(VK_CONTROL) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(VK_CONTROL)){} }; return true; } else return false; }	
	bool alt()
	{	 if(GetAsyncKeyState(VK_MENU) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(VK_MENU)){} }; return true; } else return false; }	
	bool tab()
	{	 if(GetAsyncKeyState(VK_TAB) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(VK_TAB)){} }; return true; } else return false; }	
	bool esc()
	{	 if(GetAsyncKeyState(VK_ESCAPE) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(VK_ESCAPE)){} }; return true; } else return false; }	
	bool escape()
	{	 if(GetAsyncKeyState(VK_ESCAPE) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(VK_ESCAPE)){} }; return true; } else return false; }		
	bool f1()
	{	 if(GetAsyncKeyState(VK_F1) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(VK_F1)){} }; return true; } else return false; }	
	bool f2()
	{	 if(GetAsyncKeyState(VK_F2) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(VK_F2)){} }; return true; } else return false; }		
	bool f3()
	{	 if(GetAsyncKeyState(VK_F3) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(VK_F3)){} }; return true; } else return false; }		
	bool f4()
	{	 if(GetAsyncKeyState(VK_F4) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(VK_F4)){} }; return true; } else return false; }		
	bool f5()
	{	 if(GetAsyncKeyState(VK_F5) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(VK_F5)){} }; return true; } else return false; }	

	bool num0()
	{	 if( (GetAsyncKeyState(0x30) & 0x8000) || (GetAsyncKeyState(0x60) & 0x8000))
		 { if(waitOn)
		    { while((GetAsyncKeyState(0x30) & 0x8000) || (GetAsyncKeyState(0x60) & 0x8000)){} }; return true; }
	     else return false; }
	bool num1()
	{	 if( (GetAsyncKeyState(0x31) & 0x8000) || (GetAsyncKeyState(0x61) & 0x8000))
		 { if(waitOn)
		    { while((GetAsyncKeyState(0x31) & 0x8000) || (GetAsyncKeyState(0x61) & 0x8000)){} }; return true; }
	     else return false; }
	bool num2()
	{	 if( (GetAsyncKeyState(0x32) & 0x8000) || (GetAsyncKeyState(0x62) & 0x8000))
		 { if(waitOn)
		    { while((GetAsyncKeyState(0x32) & 0x8000) || (GetAsyncKeyState(0x62) & 0x8000)){} }; return true; }
	     else return false; }
		 
	bool a()
	{	 if(GetAsyncKeyState(0x41) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(0x41)){} }; return true; } else return false; }
	bool b()
	{	 if(GetAsyncKeyState(0x42) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(0x42)){} }; return true; } else return false; }
	bool c()
	{	 if(GetAsyncKeyState(0x43) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(0x43)){} }; return true; } else return false; }
	bool d()
	{	 if(GetAsyncKeyState(0x44) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(0x44)){} }; return true; } else return false; }
	bool e()
	{	 if(GetAsyncKeyState(0x45) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(0x45)){} }; return true; } else return false; }
	bool f()
	{	 if(GetAsyncKeyState(0x46) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(0x46)){} }; return true; } else return false; }
	bool g()
	{	 if(GetAsyncKeyState(0x47) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(0x47)){} }; return true; } else return false; }
	bool h()
	{	 if(GetAsyncKeyState(0x48) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(0x48)){} }; return true; } else return false; }
	bool i()
	{	 if(GetAsyncKeyState(0x49) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(0x49)){} }; return true; } else return false; }
	bool j()
	{	 if(GetAsyncKeyState(0x4a) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(0x4a)){} }; return true; } else return false; }
	bool k()
	{	 if(GetAsyncKeyState(0x4b) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(0x4b)){} }; return true; } else return false; }
	bool l()
	{	 if(GetAsyncKeyState(0x4c) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(0x4c)){} }; return true; } else return false; }
	bool m()
	{	 if(GetAsyncKeyState(0x4d) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(0x4d)){} }; return true; } else return false; }
	bool n()
	{	 if(GetAsyncKeyState(0x4e) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(0x4e)){} }; return true; } else return false; }
	bool o()
	{	 if(GetAsyncKeyState(0x4f) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(0x4f)){} }; return true; } else return false; }
	bool p()
	{	 if(GetAsyncKeyState(0x50) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(0x50)){} }; return true; } else return false; }
	bool q()
	{	 if(GetAsyncKeyState(0x51) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(0x51)){} }; return true; } else return false; }
	bool r()
	{	 if(GetAsyncKeyState(0x52) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(0x52)){} }; return true; } else return false; }
	bool s()
	{	 if(GetAsyncKeyState(0x53) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(0x53)){} }; return true; } else return false; }
	bool t()
	{	 if(GetAsyncKeyState(0x54) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(0x54)){} }; return true; } else return false; }
	bool u()
	{	 if(GetAsyncKeyState(0x55) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(0x55)){} }; return true; } else return false; }
	bool v()
	{	 if(GetAsyncKeyState(0x56) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(0x56)){} }; return true; } else return false; }
	bool w()
	{	 if(GetAsyncKeyState(0x57) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(0x57)){} }; return true; } else return false; }
	bool x()
	{	 if(GetAsyncKeyState(0x58) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(0x58)){} }; return true; } else return false; }
	bool y()
	{	 if(GetAsyncKeyState(0x59) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(0x59)){} }; return true; } else return false; }
	bool z()
	{	 if(GetAsyncKeyState(0x5a) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(0x5a)){} }; return true; } else return false; }
	bool comma()
	{	 if(GetAsyncKeyState(0xBC) & 0x8000)
		{ if(waitOn){ while(GetAsyncKeyState(0xBC)){} }; return true; } else return false; }
	
	
	bool ctrl_c()
	{	if(ctrl() && c()) return true; else return false; }
	
	void update()
	{}

	void wait_key_n()
	{ 	cout.flush(); breakRequested = false; bool waitDone = false; 
		while(!waitDone){ if(n()) waitDone = true; else if(esc() || ctrl_c()) { waitDone = true; breakRequested = true; } } }

	bool breakWasRequested()
	{	return breakRequested; }
	
	void flush()
	{
		// clear key states
		for(int i=1; i<=255; i++)
		{ bool dummy = GetAsyncKeyState(i) & 0x8000; }
		while(kbhit()) getch();
	}
	
#endif

	//////////////////////////////////////////////////////////////////////
	//
	//		mac OSX
	//
	//////////////////////////////////////////////////////////////////////

#if defined (__APPLE__) || defined (__linux__)

	bool pressed[512];
	int key;
	int aux1, aux2;
	char buffer[32];
	bool breakRequested;

	struct termios orig_termios;

	void reset_terminal_mode()
	{
		tcsetattr(0, TCSANOW, &orig_termios);
	}
	
	void set_conio_terminal_mode()
	{
	 	struct termios new_termios;
	
	 	/* take two copies - one for now, one for later */
	 	tcgetattr(0, &orig_termios);
	 	memcpy(&new_termios, &orig_termios, sizeof(new_termios));
	
	 	/* register cleanup handler, and set the new terminal mode */
	 	atexit(reset_terminal_mode);
	 	cfmakeraw(&new_termios);
	 	tcsetattr(0, TCSANOW, &new_termios);
	}
	
	int kbhit()
	{
		struct timeval tv = { 0L, 0L };
		fd_set fds;
		FD_ZERO(&fds);
		FD_SET(0, &fds);
		return select(1, &fds, NULL, NULL, &tv);
	}
	
	int getch()
	{
		int ret;
		int r;

		r = read(0, &buffer, sizeof(buffer));
		if(r < 0)
		{	ret = r; } 
	    else if(r==1)
	    {
	    	aux1 = 0;
	    	aux2 = 0;
			ret = (int)buffer[0];
		}
		// account for escape sequence..
		else
		{
			aux1 = (int)buffer[1];
			aux2 = (int)buffer[2];
			ret = (int)buffer[0];

			if(ret==27 && aux1==91 && aux2==65) // 'UP'
				ret = 256;
			else if(ret==27 && aux1==91 && aux2==66) // 'DOWN'
				ret = 257;
			else if(ret==27 && aux1==91 && aux2==67) // 'RIGHT'
				ret = 258;
			else if(ret==27 && aux1==91 && aux2==68) // 'LEFT'
				ret = 259;
		}
		return ret;
	}

	void init()
	{
		set_conio_terminal_mode();
		for(int i=0; i<512; i++)
			pressed[i] = false;
		key = 0;
		aux1 = 0;
		aux2 = 0;
		breakRequested = false;
	}

	bool inBuffer(int keyCode)
	{
		bool ret = false;
		if(pressed[keyCode])
		{
			ret = true;
			pressed[keyCode] = false;
		}
		return ret;
	}

	bool num0()
	{	if(key==48) return true; else return false; }
	bool num1()
	{	if(key==49) return true; else return false; }
	bool num2()
	{	if(key==50) return true; else return false; }
	bool num3()
	{	if(key==51) return true; else return false; }
	bool num4()
	{	if(key==52) return true; else return false; }
	bool num5()
	{	if(key==53) return true; else return false; }
	bool num6()
	{	if(key==54) return true; else return false; }
	bool num7()
	{	if(key==55) return true; else return false; }
	bool num8()
	{	if(key==56) return true; else return false; }
	bool num9()
	{	if(key==57) return true; else return false; }

	bool a()
	{	if(key==65||key==97) return true; else return false; }	
	bool b()
	{	if(key==66||key==98) return true; else return false; }	
	bool c()
	{	if(key==67||key==99) return true; else return false; }	
	bool d()
	{	if(key==68||key==100) return true; else return false; }	
	bool e()
	{	if(key==69||key==101) return true; else return false; }	
	bool f()
	{	if(key==70||key==102) return true; else return false; }	
	bool g()
	{	if(key==71||key==103) return true; else return false; }	
	bool h()
	{	if(key==72||key==104) return true; else return false; }	
	bool i()
	{	if(key==73||key==105) return true; else return false; }	
	bool j()
	{	if(key==74||key==106) return true; else return false; }	
	bool k()
	{	if(key==75||key==107) return true; else return false; }	
	bool l()
	{	if(key==76||key==108) return true; else return false; }	
	bool m()
	{	if(key==77||key==109) return true; else return false; }	
	bool n()
	{	if(key==78||key==110) return true; else return false; }	
	bool o()
	{	if(key==79||key==111) return true; else return false; }
	bool p()
	{	if(key==80||key==112) return true; else return false; }	
	bool q()
	{	if(key==81||key==113) return true; else return false; }	
	bool r()
	{	if(key==82||key==114) return true; else return false; }	
	bool s()
	{	if(key==83||key==115) return true; else return false; }	
	bool t()
	{	if(key==84||key==116) return true; else return false; }	
	bool u()
	{	if(key==85||key==117) return true; else return false; }	
	bool v()
	{	if(key==86||key==118) return true; else return false; }	
	bool w()
	{	if(key==87||key==119) return true; else return false; }	
	bool x()
	{	if(key==88||key==120) return true; else return false; }	
	bool y()
	{	if(key==89||key==121) return true; else return false; }	
	bool z()
	{	if(key==90||key==122) return true; else return false; }	

	bool esc()
	{	if(key==27) return true; else return false; }
	bool escape()
	{	if(key==27) return true; else return false; }

	bool up()
	{	if(key==256) return true; else return false; }
	bool down()
	{	if(key==257) return true; else return false; }
	bool right()
	{	if(key==258) return true; else return false; }
	bool left()
	{	if(key==259) return true; else return false; }

	bool space()
	{	if(key==32) return true; else return false; }
	bool ret()
	{	if(key==13) return true; else return false; }
	bool tab()
	{	if(key==9) return true; else return false; }
	bool backspace()
	{	if(key==127) return true; else return false; }

	bool ctrl_c()
	{	if(key==3) return true; else return false; }

	// DEBUG
	int getKeyCodePressed()
	{
		if(key>=0)
			cout << "pressed = " << key << ", aux1 = " << aux1 << ", aux2 = " << aux2 << "\r\n";
		return key;
	}

	void update()
	{
		if(kbhit())
		{
			key = getch();
			if(key>=0) 
				pressed[key] = true;
		}
		else
			key = -1;
	}

	void wait_key_n()
	{ 	cout.flush(); breakRequested = false; bool waitDone = false; 
		while(!waitDone){ update(); if(n()) waitDone = true; else if(esc() || ctrl_c()) { waitDone = true; breakRequested = true; } } }

	bool breakWasRequested()
	{	return breakRequested; }

	void flush()
	{}

#endif

}

////////////////////////////////////////////////////////////////////////////////////////

namespace scr
{


#if defined (_WIN32)

	void clear()
	{
		system("cls");
	}

	void out(const std::string &str)
	{
		cout << str;
	}

	void println(const std::string &str)
	{
		cout << str << "\r\n";
	}

#endif

#if defined (__APPLE__) || defined (__linux__)

	void clear()
	{
		system("clear");
	}

	void out(const std::string &str)
	{
		cout << str;
	}

	void println(const std::string &str)
	{
		cout << str << "\r\n";
	}

#endif


}

//////////// MISC ///////////////////////////

std::string toStr(std::string &str)
{
	return str;
}

std::string toStr(int n)
{
	std::stringstream ss; ss << n; return ss.str();
}

std::string toStr(double n)
{
	std::stringstream ss; ss << n; return ss.str();
}

std::string toStr(float n)
{
	std::stringstream ss; ss << n; return ss.str();
}

std::string toStr(long n)
{
	std::stringstream ss; ss << n; return ss.str();
}

//////////// timer //////////////////////////

namespace timer
{
	

#ifdef _WIN32

double timerFrequency;
unsigned __int64 startTime;
unsigned __int64 endTime;
double timeDifferenceInMilliseconds;

void resetTimer()
{
	unsigned __int64 freq;
	QueryPerformanceFrequency((LARGE_INTEGER*)&freq);
	timerFrequency = (1.0/freq);
	QueryPerformanceCounter((LARGE_INTEGER *)&startTime);
}

double getElapsedTime()
{
	QueryPerformanceCounter((LARGE_INTEGER *)&endTime);
	timeDifferenceInMilliseconds = ( ((endTime-startTime) * timerFrequency) ) * 1000.0;
	return timeDifferenceInMilliseconds;
}

#endif

#if defined (__APPLE__) || defined (__linux__)

struct timeval tv;
unsigned long startTime;
unsigned long endTime;
double timeDifferenceInMilliseconds;

unsigned long getTimeMicroSec()
{
	gettimeofday(&tv,NULL);
	unsigned long timeMicroSec = 1000000 * tv.tv_sec + tv.tv_usec;
	return timeMicroSec;	
}

void resetTimer()
{
	startTime = getTimeMicroSec();
}

double getElapsedTime()
{
	endTime = getTimeMicroSec();
	timeDifferenceInMilliseconds = ((double)(endTime - startTime)) / 1000.00;
	return timeDifferenceInMilliseconds;
}

#endif

}


/////////////// filesystem //////////////////

namespace file
{

	#ifdef _WIN32

	std::string getpwd()
	{
		TCHAR currentDir[MAX_PATH];
		GetCurrentDirectory( MAX_PATH, currentDir );
		string strReturn = currentDir;
		return strReturn;
	}
	
	// get all file names in dirPath
	std::vector<std::string> getFileNamesInDir(const std::string &dirPath)
	{
		std::vector<std::string> names;
		
		char search_path[200];
		sprintf(search_path, "%s/*.*", dirPath.c_str());
		WIN32_FIND_DATA fd; 
		HANDLE hFind = ::FindFirstFile(search_path, &fd); 
		if(hFind != INVALID_HANDLE_VALUE) { 
			do { 
				// read all (real) files in current dirPath
				// , delete '!' read other 2 default dirPath . and ..
				if(! (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) ) {
					names.push_back(fd.cFileName);
				}
			}while(::FindNextFile(hFind, &fd)); 
			::FindClose(hFind); 
		}
		
		return names;
	}
	
	
	std::vector<std::string> getDirNamesInDir(const std::string &folder)
	{
		std::vector<std::string> names;
		
			char search_path[512];
			sprintf(search_path, "%s/*.*", folder.c_str());
			WIN32_FIND_DATA fd; 
			HANDLE hFind = ::FindFirstFile(search_path, &fd); 
			if(hFind != INVALID_HANDLE_VALUE) { 
				do { 
					// read all (real) files in current folder
					// , delete '!' read other 2 default folder . and ..
					if( (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) )
					{
						string fileName = fd.cFileName;
						if(fileName!="." && fileName !="..")
							names.push_back(fd.cFileName);
					}
				}while(::FindNextFile(hFind, &fd)); 
				::FindClose(hFind); 
			}
		
		return names;
	}
	
	#endif
	
	#if defined (__APPLE__) || defined (__linux__)
	
	std::string getpwd()
	{
		// get the current file path
		char buffer[512];
		string pwd = string(getcwd(buffer, 512));
		return pwd;
	}

	// get all file names in dirPath
	std::vector<std::string> getFileNamesInDir(const std::string &dirPath)
	{
		std::string tempPWD = getpwd();
		std::vector<std::string> names;
		
			chdir(dirPath.c_str());
			DIR *dp;
			struct dirent *ep;
			dp = opendir(string(dirPath).c_str());
		
			if(dp!=NULL)
			{
				while( (ep=readdir(dp)) )
				{
					struct stat buf;
					stat(ep->d_name, &buf);
					if(S_ISREG(buf.st_mode))
						names.push_back(string(ep->d_name));
				}
				closedir(dp);
			}
			else
				cout << "Error opening directory: " << dirPath << endl;
			chdir(tempPWD.c_str());
		
		return names;
	}
	
	
	std::vector<std::string> getDirNamesInDir(const std::string &folder)
	{
		std::string tempPWD = getpwd();
		std::vector<std::string> names;
	
			chdir(folder.c_str());
			DIR *dp;
			struct dirent *ep;
			dp = opendir(folder.c_str());
		
			if(dp!=NULL)
			{
				while( (ep=readdir(dp)) )
				{
					struct stat buf;
					stat(ep->d_name, &buf);
					string dirName = ep->d_name;
					if(S_ISDIR(buf.st_mode) && dirName!="." && dirName !="..")
						names.push_back(dirName);
				}
				(void)closedir(dp);
			}
			else
				cout << "Error opening directory: " << folder << endl;
			chdir(tempPWD.c_str());
	
		return names;
	}
	
	#endif
	
	bool makeDir(const std::string &dirPath)
	{
		bool result = true;
		#ifdef _WIN32
			CreateDirectory( dirPath.c_str(), NULL );
		#elif
			int result = mkdir(dirPath.c_str(), 0755);
		#endif		
	}

	bool copyFile(const std::string &sourcePath, const std::string &destPath)
	{
		bool result = true;
		ifstream source(sourcePath.c_str(), std::ios::binary);
		ofstream dest(destPath.c_str(), std::ios::binary | std::ios_base::trunc);
		if(!source)
		{
			cout << "Error opening: " << sourcePath << endl;
			result = false;
		}
		if(!dest)
		{
			cout << "Error opening: " << destPath << endl;
			result = false;
		}
		else
			dest << source.rdbuf();
		source.close();
		dest.close();
		return result;
	}

	bool dirExists(const std::string &dirPath)
	{
		string path = dirPath;
		#if _WIN32
			DWORD dwAttrib = GetFileAttributes(path.c_str());
			return (bool)(dwAttrib != INVALID_FILE_ATTRIBUTES && 
				(dwAttrib & FILE_ATTRIBUTE_DIRECTORY));
		#elif
			bool result;
			DIR* dir = opendir(path.c_str());
			if (dir)
			{
				result = true;
				/* Directory exists. */
				closedir(dir);
			}
			else if (ENOENT == errno)
			{
				result = false;
				/* Directory does not exist. */
			}
			else
			{
				result = false;
				cout << "Error - opendir(): " << path << endl;
				/* opendir() failed for some other reason. */
			}
			return result;
		#endif
	}

} // namespace file


//////////// initialization /////////////////

#if defined (_WIN32)

	void hirolibInit()
	{ 
		kbd::waitOn = true;
		kbd::flush();
		timer::resetTimer();
	}

#endif

#if defined (__APPLE__) || defined (__linux__)


void hirolibInit()
{	
	kbd::init();
	timer::resetTimer();
}

#endif

////////////////////////////////////////////

#endif // HIROLIB_H
